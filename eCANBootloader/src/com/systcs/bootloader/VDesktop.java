package com.systcs.bootloader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.systcs.can.IXXAT;


public class VDesktop extends ApplicationWindow {
	
	public static enum ViewEvent {
		APP_LAUNCHED, 			//Application Window set up is complete after launch by OS
		APP_CLOSING,  			//Application Window closing triggered by OS before closing window
		CAN_OPEN_REQUESTED, 	//User has requested opening USB2CAN driver using OPEN Button in Tools Tab for CAN communication
		CAN_TEST_REQUESTED, 	//User has requested testing CAN communication using Test Button in Tools Tab
		APP_CLOSE_REQUESTED, 	//Application Window closing has been requested either by user closing application
		CAN_CLOSE_REQUESTED, 	//Application has requested USB2CAN driver to close
		FILE_SELECTED, LOAD_REQUESTED	
	}
	
	public static void postEvent(ViewEvent event) {
		CListener.onEvent(event);
	}
	
	public static Shell theShell; //Desktop shell
	
	private static Button load;
	
	public static FontRegistry fonts;
	public static String fontBVS20 = "fontBVS20";
	public static String fontBVS12B = "fontBVS12B";
	public static String fontBVS14B = "fontBVS14B";
	public static String fontBVS12N = "fontBVS12N";
	
	public static IconsRegister icons;

	private Cursor cursorDefault; //Cursor from system display
	private Cursor cursorWait; //User cursor used when app is busy
	
	public static Text logWindow;	//Text Window that logs all messages from the application
	private static File logFile;	//File that logs all messages from the application
	private static BufferedWriter logWriter;
	
	public static String fileSelected = null;

	public VDesktop() {
		super(null);
		// TODO Auto-generated constructor stub
		addMenuBar();
		addCoolBar(SWT.FLAT);
		addStatusLine();
		
		String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		logFile = new File("./TB_"+timeLog+".txt");
		try {
			String path = logFile.getCanonicalPath();
			//log.append("Log file is: " + path + "\n");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			logWriter = new BufferedWriter(new FileWriter(logFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logToFile("Log File: \n");
	}

	@Override
	protected void configureShell(Shell shell) {
		// TODO Auto-generated method stub
		super.configureShell(shell);
		
		fonts = JFaceResources.getFontRegistry();
		fonts.put(fontBVS20, new Font(shell.getDisplay(), 
                "Bitstream Vera Sans", 20, SWT.NORMAL).getFontData());
		fonts.put(fontBVS12B, new Font(shell.getDisplay(), 
                "Bitstream Vera Sans", 12, SWT.BOLD).getFontData());
		fonts.put(fontBVS14B, new Font(shell.getDisplay(), 
                "Bitstream Vera Sans", 14, SWT.BOLD).getFontData());
		fonts.put(fontBVS12N, new Font(shell.getDisplay(), 
                "Bitstream Vera Sans", 12, SWT.NORMAL).getFontData());
		icons = new IconsRegister(shell.getDisplay());
		
		cursorDefault = shell.getCursor();
		cursorWait = new Cursor(shell.getDisplay(), SWT.CURSOR_WAIT);
		
		VDesktop.theShell = shell;	//Sets the static member that can be accessed by dialogs in any context
	}
	
	
	@Override
	protected Control createContents(Composite parent) {
		// TODO Auto-generated method stub
		Shell parentShell = parent.getShell();
		parentShell.setText("ECAN BOOTLOADER");
		parent.setSize(800, 700);
		
		//This view is the container for 3 components - the controls group, the indicators and the log text window
		Composite view = new Composite(parent, SWT.NONE);
		view.setLayout(new GridLayout(1, false)); //Grid of 1 column
		
		createTools(view);
		
		//Text window logs all the messages from the application
		logWindow = new Text(view, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
		logWindow.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		displayStatus(" READY");	
		
		getShell().addShellListener(new ShellAdapter() {
	        @Override
	        public void shellActivated(ShellEvent shellevent) {
	        	if (CListener.onLine) {
		        	if (IXXAT.isOpen != true) {//Execute this callback only if CAN is not initialized
		        		getShell().setCursor(cursorWait);
		        		postEvent(ViewEvent.CAN_OPEN_REQUESTED);
		        	}
	        	}
	        }
	    }); 
			
		return parent;
		//return super.createContents(parent);
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		// TODO Auto-generated method stub
		//super.setShellStyle(newShellStyle);
		super.setShellStyle(getShellStyle() & (~SWT.RESIZE)); //Prevent resize of window
	}



	@Override
	protected StatusLineManager createStatusLineManager() {
		// TODO Auto-generated method stub
		return super.createStatusLineManager();
	}

	public void run() {
		setBlockOnOpen(true);
		open(); //Does not return from this till window is closed
		
		//Close log file
		try {
			logWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cursorWait.dispose();
		Display.getCurrent().dispose();
	}


	public static class IconsRegister {
		
		static ImageRegistry icons;
	    
	    IconsRegister(Display display) {
	        icons = new ImageRegistry(display);
	        //add("left.gif");
	        //add("go.gif");
	        //add("home.gif");
	        //add("right.gif");
	        //add("up.gif");
	        //add("down.gif");
	        //add("move_to.gif");
	        //add("lock.ico");
	        //add("cw.png");
	        //add("ccw.png");
	        //add("stop.ico");
	        ////add("rotate.ico");
	        //add("rotate.gif");
	        //add("right.ico");
	        //add("stop.gif");
	    }
	    
	    void add(String fileName) {
	        ImageDescriptor id = ImageDescriptor.createFromFile(this.getClass(), "Icons/"+fileName);
	        icons.put(fileName, id);
	    }
	    
	    static public Image get(String fileName) {
	    	Image img = icons.get(fileName);
	        return icons.get(fileName);
	    }

	}
	
	public void displayStatus(String message) {
		getStatusLineManager().setMessage(message);
	}
	
	public static void logToFile(String s) {
		try {
			logWriter.write(s);
			logWriter.append(System.lineSeparator());
			logWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void log(String s) {
		logWindow.append(s);
		logToFile(s);
		
	}
	
	public static void closeLogFile() {
		if (logWriter != null) {
			try {
				logWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void retryCanOpen() {
		// TODO Auto-generated method stub
		MessageDialog dialog = new MessageDialog(getShell(), null, null,
			    "Cannot detect IXXAT USB2CAN device", MessageDialog.ERROR, new String[] { "Retry",
			  "Exit" }, 0);
			int result = dialog.open();
			if (result == 0) {
				 //postEvent(ViewEvent.CAN_OPEN_REQUESTED);
			} else 
			{
				//postEvent(ViewEvent.APP_CLOSE_REQUESTED);
			}; 
	}

	public void retryCanTest() {
		// TODO Auto-generated method stub
		MessageDialog dialog = new MessageDialog(getShell(), null, null,
			    "Cannot detect Joint Controller", MessageDialog.ERROR, new String[] { "Retry",
			  "Exit" }, 0);
			int result = dialog.open();
			if (result == 0) {
				 //postEvent(ViewEvent.CAN_TEST_REQUESTED);
			} else 
			{
				//postEvent(ViewEvent.APP_CLOSE_REQUESTED);
			};
	}
	
	private void createTools(final Composite parent) {
		GridLayout gl; GridData gd;
		
		Group aGrp = new Group(parent, SWT.NONE);
		gl = new GridLayout(6, false);
		aGrp.setLayout(gl);
		aGrp.setText(" FILE SELECTION ");
		gd = new GridData(GridData.HORIZONTAL_ALIGN_CENTER | GridData.FILL_HORIZONTAL);
		gd.widthHint = 300;
		aGrp.setLayoutData(gd);
		
		Button selectFile = new Button(aGrp, SWT.PUSH);
		selectFile.setText(" LOAD FILE "
				+ "");
		selectFile.setToolTipText("Select the File for loading");
		selectFile.setLayoutData(new GridData(GridData.CENTER));
		selectFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				fileSelected = null;
				selectFile();
				if (fileSelected != null)
					VDesktop.postEvent(ViewEvent.FILE_SELECTED);
			}			
		});
		
		load = new Button(aGrp, SWT.PUSH);
		load.setText(" STREAM FILE");
		load.setToolTipText("Stream the file over CAN");
		load.setLayoutData(new GridData(GridData.CENTER));
		load.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				VDesktop.postEvent(ViewEvent.LOAD_REQUESTED);
				
			}			
		});	
		load.setEnabled(false); //Activate only after selecting file
		
	}
	
	private void selectFile() {
		// File standard dialog
	    FileDialog fileDialog = new FileDialog(theShell);
	    // Set the text
	    fileDialog.setText("Select File");
	    // Set filter on .hex files
	    fileDialog.setFilterExtensions(new String[] { "*.hex" });
	    // Put in a readable name for the filter
	    fileDialog.setFilterNames(new String[] { "Hexfiles(*.hex)" });
	    // Open Dialog and save result of selection
	    String selected = fileDialog.open();
	    if (selected != null) {
	    	fileSelected = selected;
	    	log("\n File selected: " + fileSelected);
	    	File f = new File(fileSelected);
			int length = (int) f.length();
			log("\n File size is " + Integer.toString(length) + " Bytes \n");
	    }
	    else {
	    	log("\n No file selected \n");
	    }
	}

	public static void enableLoad() {
		// TODO Auto-generated method stub
		load.setEnabled(true);
	}

	public static void displayByte(int sent) {
		// TODO Auto-generated method stub
		String s = String.format("%02x ", sent);
		log(s);
	}

}

