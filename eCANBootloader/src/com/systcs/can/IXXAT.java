/**
 * Driver.java
 * Copyright Systemantics 2014
 * Created on Mar 4, 2014 by G J Raju
 */
package com.systcs.can;

import java.util.Arrays;

import com.systcs.bootloader.CListener;

import de.ixxat.vci3.IVciDevice;
import de.ixxat.vci3.IVciDeviceManager;
import de.ixxat.vci3.IVciEnumDevice;
import de.ixxat.vci3.VciDeviceCapabilities;
import de.ixxat.vci3.VciDeviceInfo;
import de.ixxat.vci3.VciException;
import de.ixxat.vci3.VciServer;
import de.ixxat.vci3.bal.BalFeatures;
import de.ixxat.vci3.bal.BalSocketInfo;
import de.ixxat.vci3.bal.IBalObject;
import de.ixxat.vci3.bal.IBalResource;
import de.ixxat.vci3.bal.can.CanBitrate;
import de.ixxat.vci3.bal.can.CanCapabilities;
import de.ixxat.vci3.bal.can.CanChannelStatus;
import de.ixxat.vci3.bal.can.CanLineStatus;
import de.ixxat.vci3.bal.can.CanMessage;
import de.ixxat.vci3.bal.can.ICanChannel;
import de.ixxat.vci3.bal.can.ICanControl;
import de.ixxat.vci3.bal.can.ICanMessageReader;
import de.ixxat.vci3.bal.can.ICanMessageWriter;
import de.ixxat.vci3.bal.can.ICanSocket;

/**
 * @author G J Raju
 *
 */
public class IXXAT {
	
	static IBalObject oBalObject = null; 
	
	static ICanControl       oCanControl     = null;
    static ICanSocket        oCanSocket      = null;
    //ICanScheduler     oCanScheduler   = null;
    static ICanChannel       oCanChannel     = null;
    
    static ICanMessageReader oCanMsgReader   = null;
    static ICanMessageWriter oCanMsgWriter   = null;
    
    public static Boolean isOpen = false;
	
	public static enum CANDriverEvent {
		CAN_EXCEPTION,
		CAN_OPENED,
		CAN_TEST_PASSED,
		CAN_TEST_FAILED,
		CAN_CLOSED, 
		CAN_READ_FAILED, 
		CAN_WRITE_DONE, 
		CAN_READ_PASSED, 
		CAN_MSG_FAILED
	}
	
	public static void postEvent(CANDriverEvent event) {
		CListener.onEvent(event);
	}

	public static void open() {
		// Create VCI Server Object
	    VciServer         oVciServer      = null;
	    IVciDeviceManager oDeviceManager  = null;
	    IVciEnumDevice    oVciEnumDevice  = null;
	    IVciDevice        oVciDevice      = null;
	    
	    VciDeviceInfo     aoVciDeviceInfo[] = null;
	    
	    // Initialize VCI Server Object
	    try
	    {
	      // Create VCI Server Object
	      oVciServer = new VciServer();

	      // Print Version Number
	      //System.out.println(oVciServer.GetVersion());
	    
	      // Open VCI Device Manager
	      oDeviceManager = oVciServer.GetDeviceManager();
	      
	      // Open VCI Device Enumerator
	      oVciEnumDevice = oDeviceManager.EnumDevices();

	      //System.out.print("Wait for VCI Device Enum Event: ...");
	      try
	      {
	        oVciEnumDevice.WaitFor(1000);
	        //System.out.println("... change detected!");
	      }
	      catch(Throwable oException)
	      {
	        //System.out.println("... NO change detected!");
	      }
	      VciDeviceInfo oVciDeviceInfo    = null;
	      if (oVciEnumDevice != null) {
		      try
	          {
	            // Try to get next device
	            oVciDeviceInfo = oVciEnumDevice.Next();
	            oVciDevice = oDeviceManager.OpenDevice(oVciDeviceInfo.m_qwVciObjectId);
	            oVciEnumDevice.Reset();
	          }
	          catch(Throwable oException)
	          {
	            // Last device reached?
	            oVciDeviceInfo = null;
	          }
	      }
	      	      
	      // Open BAL Object
	      oBalObject = oVciDevice.OpenBusAccessLayer();

	      // Free VciEnumDevice, DeviceManager and VCI Server which are not longer needed
	      oVciEnumDevice.Dispose();
	      oVciEnumDevice = null;
	      oDeviceManager.Dispose();
	      oDeviceManager = null;
	      oVciServer.Dispose();
	      oVciServer     = null; 
	    }
	    catch(Throwable oException)
	    {
	      if (oException instanceof VciException)
	      {
	        VciException oVciException = (VciException) oException;
	        postEvent(CANDriverEvent.CAN_EXCEPTION);
	        //System.err.println("VciException: " + oVciException + " => " + oVciException.VciFormatError());
	      } 
	      else {
	    	  postEvent(CANDriverEvent.CAN_EXCEPTION);
	        //System.err.println("Exception: " + oException);
	      }
	      return;
	    }
	    
	    // release all references
	    //System.out.println("Cleaning up Interface references to VCI Device");
	    try
	    {
	      oVciDevice.Dispose();
	    } 
	    catch (Throwable oException){} 
	    finally
	    {
	      oVciDevice = null;
	    }
	    
	    short wSocketNumber = 0;
		 
	    // Open CAN Control and CAN Channel
	    try
	    {
	      IBalResource  oBalResource  = null;
	      BalFeatures   oBalFeatures  = null;
	      
	      //System.out.println("Using Socket Number: " + wSocketNumber);
	      
	      oBalFeatures = oBalObject.GetFeatures();
	      //System.out.println("BAL Features: " + oBalFeatures);
	      
	      // Socket available?
	      if(oBalFeatures.m_wBusSocketCount > wSocketNumber)
	      {
	        // Ensure CAN Controller Type
	        if(oBalFeatures.m_awBusType[wSocketNumber] == VciDeviceCapabilities.VCI_BUS_CAN)
	        {
	          // Get CAN Control
	          try
	          {
	            oBalResource  = oBalObject.OpenSocket(wSocketNumber, IBalResource.IID_ICanControl);
	            oCanControl   = (ICanControl)oBalResource;
	            oBalResource  = null;
	          }
	          catch(Throwable oException)
	          {
	            if (oException instanceof VciException)
	            {
	              VciException oVciException = (VciException) oException;
	              System.err.println("Open Socket(IID_ICanControl), VciException: " + oVciException + " => " + oVciException.VciFormatError());
	            } 
	            else
	              System.err.println("Open Socket(IID_ICanControl), exception: " + oException);
	          }

	          // Get CAN Socket
	          try
	          {
	            oBalResource  = oBalObject.OpenSocket(wSocketNumber, IBalResource.IID_ICanSocket);
	            oCanSocket    = (ICanSocket)oBalResource;
	            oBalResource  = null;
	          }
	          catch(Throwable oException)
	          {
	            if (oException instanceof VciException)
	            {
	              VciException oVciException = (VciException) oException;
	              System.err.println("Open Socket(IID_ICanSocket), VciException: " + oVciException + " => " + oVciException.VciFormatError());
	            } 
	            else
	              System.err.println("Open Socket(IID_ICanSockets), exception: " + oException);
	          }
	        }
	        else
	          System.err.println("Socket No. " + wSocketNumber + " is not a \"VCI_BUS_CAN\"");
	      }
	      else
	        System.err.println("Socket No. " + wSocketNumber + " is not a available!");
	    }
	    catch(Throwable oException)
	    {
	      if (oException instanceof VciException)
	      {
	        VciException oVciException = (VciException) oException;
	        System.err.println("VciException: " + oVciException + " => " + oVciException.VciFormatError());
	      } 
	      else
	        System.err.println("Exception: " + oException);
	    }

	    // Open CAN
	    try
	    {
	      // Create CAN Channel
	      if(oCanSocket != null)
	      {
	        BalSocketInfo   oBalSocketInfo    = null;
	        CanCapabilities oCanCapabilities  = null;
	        CanLineStatus   oCanLineStatus    = null;
	  
	        oBalSocketInfo = oCanSocket.GetSocketInfo();
	        //System.out.println("BAL Socket Info: " + oBalSocketInfo);
	  
	        oCanCapabilities = oCanSocket.GetCapabilities();
	        //System.out.println("CAN Capabilities: " + oCanCapabilities);
	  
	        oCanLineStatus = oCanSocket.GetLineStatus();
	        //System.out.println("CAN Line Status: " + oCanLineStatus);
	        
	        //System.out.println("Creating CAN Channel");
	        oCanChannel = oCanSocket.CreateChannel(false);
	      }
	  
	      // Configure, start Channel and Query Reader and Writer
		      if(oCanChannel != null)
		      {
		        //System.out.println("Initializing CAN Message Channel");
		        oCanChannel.Initialize(Short.MAX_VALUE, Short.MAX_VALUE);
		        oCanChannel.Activate();
		        
		        //System.out.println("Query Message Reader");
		        oCanMsgReader = oCanChannel.GetMessageReader();
		        
		        //System.out.println("Query Message Writer");
		        oCanMsgWriter = oCanChannel.GetMessageWriter();
		      }
		  
	      // Configure and start CAN Controller
	      if(oCanControl != null)
	      {
	        CanBitrate        oCanBitrate       = new CanBitrate(CanBitrate.Cia1000KBit);
	        CanChannelStatus  oChanStatus       = null;
	        CanLineStatus     oLineStatus       = null;
	        
	        // Get Line Status
	        oLineStatus = oCanControl.GetLineStatus();
	        //System.out.println("CAN Line Status: " + oLineStatus);
	                
	        //System.out.println("Starting CAN Controller with 500 kBAUD");
	        oCanControl.InitLine(ICanControl.CAN_OPMODE_STANDARD |
	                             ICanControl.CAN_OPMODE_EXTENDED, 
	                             oCanBitrate);
	        
	        // Start
	        oCanControl.StartLine();
	  
	        // Wait for controller
	        Thread.sleep(250);
	        
	        // Get CAN Channel Status
	        oChanStatus = oCanChannel.GetStatus();
	        //System.out.println("CAN Channel Status: " + oChanStatus);
	        //System.out.println("CAN Line Status: " + oChanStatus.m_oCanLineStatus);
	        //System.out.println("");
	      }
	    }
	    catch(Throwable oException)
	    {
	      if (oException instanceof VciException)
	      {
	        VciException oVciException = (VciException) oException;
	        System.err.println("VciException: " + oVciException + " => " + oVciException.VciFormatError());
	      } 
	      else
	        System.err.println("Exception: " + oException);
	    }
	    
	    if(oCanControl != null)
	    {
	      try
	      { 
	        // Get Line Status
	        CanLineStatus oLineStatus = null;
	        oLineStatus = oCanControl.GetLineStatus();
	        //System.out.println("CAN Line Status: " + oLineStatus);
	      }
	      catch(Throwable oException)
	      {
	        if (oException instanceof VciException)
	        {
	          VciException oVciException = (VciException) oException;
	          System.err.println("Get CAN Line Status, VciException: " + oVciException + " => " + oVciException.VciFormatError());
	        } 
	        else
	          System.err.println("Get CAN Line Status, Exception: " + oException);
	      }
	    }
	    
	    isOpen = true;
	    postEvent(CANDriverEvent.CAN_OPENED);
	}
	 
	
	public static void test() {  
		
      // Write Test Message
      if(oCanMsgWriter != null)
      { 	
    	byte[] testBytes = new byte[]{0x00, 0x14};
        write(0, testBytes);	//Broadcasting
      }
      try {
		Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      // Read Response to Test Message
      if (oCanMsgReader != null)
      {
        byte[] readBytes = read();
        //byte[] readBytes = new byte[]{0x00, 0x14 , 0x54, 0x45, 0x53, 0x54};
        if (readBytes != null) {
        	postEvent(CANDriverEvent.CAN_READ_PASSED);
        	//VDesktop.log.append("Read: " + ((Integer)(readBytes.length)).toString() + " bytes \n");
        	//VDesktop.log.append("Read: "+ new BigInteger(1, readBytes).toString(16) + "\n");
        	if (Arrays.equals(readBytes, new byte[]{0x00, 0x14, 0x54, 0x45, 0x53, 0x54}))
	        	postEvent(CANDriverEvent.CAN_TEST_PASSED);
        	else
        		postEvent(CANDriverEvent.CAN_TEST_FAILED);
        }
        else {
        	postEvent(CANDriverEvent.CAN_READ_FAILED);
        }
      }
      else
    	  postEvent(CANDriverEvent.CAN_MSG_FAILED);
	}
	
	    
	public static void write(int id, byte[] data) {
		try
	    {
	      CanMessage  oTxCanMsg = new CanMessage();
	      boolean     fTimedOut = false;
	
	      // PreSet CAN Message
	      //oTxCanMsg.m_abData        = new byte[]{0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, (byte)0x88};
	      //oTxCanMsg.m_bDataLength   = 8;
	      oTxCanMsg.m_abData        = data;
	      oTxCanMsg.m_bDataLength   = (byte) data.length;
	      //oTxCanMsg.m_dwIdentifier  = 0;
	      oTxCanMsg.m_dwIdentifier  = id;	//Not broadcasting but to specific id
	      oTxCanMsg.m_dwTimestamp   = 0; // No Delay
	      oTxCanMsg.m_fExtendedFrameFormat        = false;
	      oTxCanMsg.m_fRemoteTransmissionRequest  = false;
	      oTxCanMsg.m_fSelfReception              = false;
	      
	      //System.out.println("\nPress <Enter> to exit transmission mode\n");
	
	        try
	        {
	          // Write CAN Message
	          // If WriteMessage fails, it throws an exception
	          oCanMsgWriter.WriteMessage(oTxCanMsg);
	          postEvent(CANDriverEvent.CAN_WRITE_DONE);
	          if(fTimedOut)
	          {
	            System.out.print("\n");
	            fTimedOut = false;
	          }
	          //System.out.println("No: " + qwMsgNo + " " + oCanMsg); //Scroll Mode
	          //System.out.print("\rNo: " + qwMsgNo + " " + oTxCanMsg + "  "); //Overwrite Mode
	
	        }
	        catch(Throwable oException)
	        {
	          //System.out.print("\r" + oException);
	          //Wait for empty space in the FIFO
	          try 
	          {
	            oCanMsgWriter.WaitFor(500);
	          }
	          catch(Throwable oThrowable)
	          {
	            if(!fTimedOut)
	            {
	              //System.out.print("\n");
	              fTimedOut = true;
	            }
	            //System.out.print(".");
	          }
	        }
	    }
	    catch(Throwable oException)
	    {
	      if (oException instanceof VciException)
	      {
	        VciException oVciException = (VciException) oException;
	        //System.err.println("VciException: " + oVciException + " => " + oVciException.VciFormatError());
	      } 
	      else
	    	  ;
	        //System.err.println("Exception: " + oException);
	    }
	}
	
	public static byte[] read() {
		
		try {	
			CanMessage  oCanMsg   = new CanMessage();
			boolean fTimedOut = false;
			boolean nonDataFrame = false;
			
			do { //Stay in this loop till a data frame is read or timed out
		        try
		        {
		          // Read CAN Message
		          // If ReadMessage fails, it throws an exception
		          oCanMsg = oCanMsgReader.ReadMessage(oCanMsg);
		
		          byte[] readBytes = new byte[oCanMsg.m_bDataLength];
		          System.arraycopy(oCanMsg.m_abData, 0, readBytes, 0, readBytes.length);
		          return readBytes;
		        }
		        catch(Throwable oException)
		        {
		        	if (oException instanceof IllegalArgumentException) {
		        		//Frame was  not a data frame and got discarded
		        		//Continue in loop to read next frame
		        		nonDataFrame = true;
		        	}
		        	else {
			        	//Wait for a new data frame in the queue
			            try 
			            {
			              oCanMsgReader.WaitFor(100); //This throws an exception when the time elapses
			            }
			            catch(Throwable oThrowable)
			            {
			              if(!fTimedOut)
			              {
			                fTimedOut = true;
			              }
			            }
		        	}
		        }	
			}
	        while (!fTimedOut);
		}
		catch(Throwable oException)
	    {
	      if (oException instanceof VciException)
	      {
	        VciException oVciException = (VciException) oException;
	        //System.err.println("VciException: " + oVciException + " => " + oVciException.VciFormatError());
	      } 
	      else
	    	  ;
	        //System.err.println("Exception: " + oException);
	    }		
		return null;
	}
	
	public static void close() {
		
		// Dispose CAN Message Writer
		if (oCanMsgWriter != null) {
			try {
				oCanMsgWriter.Dispose();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			oCanMsgWriter = null;
		}
        
        // Dispose CAN Message Reader
	    if(oCanMsgReader != null)
	    {
	      try {
	    	  oCanMsgReader.Dispose();
	      } catch (Throwable e) {
	    	  // TODO Auto-generated catch block
	    	  e.printStackTrace();
	      }
	        oCanMsgReader = null;
	    }
	      
	    try
	    {
	      oBalObject.Dispose();
	    } 
	    catch (Throwable oException){} finally
	    {
	      oBalObject = null;
	    }

		
		// Stop CAN Controller
	    if(oCanControl != null)
	    {
	      try
	      { 
	        //System.out.println("Stopping CAN Controller");
	        oCanControl.StopLine();
	        oCanControl.ResetLine();
	      }
	      catch(Throwable oException)
	      {
	        if (oException instanceof VciException)
	        {
	          VciException oVciException = (VciException) oException;
	          System.err.println("Reset CAN Controller, VciException: " + oVciException + " => " + oVciException.VciFormatError());
	        } 
	        else
	          System.err.println("Reset CAN Controller, Exception: " + oException);
	      }
	    }
	    
	    // release all references
	    //System.out.println("Cleaning up CAN Interface references");
	    try
	    {
	      oCanControl.Dispose();
	    } 
	    catch (Throwable oException){} 
	    finally
	    {
	      oCanControl = null;
	    }
	    try
	    {
	      oCanSocket.Dispose();
	    } 
	    catch (Throwable oException){} 
	    finally
	    {
	      oCanSocket = null;
	    }
	    try
	    {
	      oCanChannel.Dispose();
	    } 
	    catch (Throwable oException){}
	    finally
	    {
	      oCanChannel = null;
	    };
	}
}	


