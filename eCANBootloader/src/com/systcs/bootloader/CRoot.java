package com.systcs.bootloader;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;


public class CRoot {
	
	static VDesktop desktop;	//MVC architecture View
	static MCANPort port;		//MVC architecture Model
	static CListener listener;	//MVC architecture Controller

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		desktop = new VDesktop();			//Create the View
		listener = new CListener(desktop);	//Connect View to the Controller
				
		try {
			System.setErr(new PrintStream(new FileOutputStream("TB_Errors.txt", true), true));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		desktop.run();	//Launch the View
	}
	
	public static void setupCANPort() {	//Set up Model after View has been successfully launched

		port = new MCANPort();		//Create the Model
		listener.setModel(port);	//Connect the Model to the Controller
	}

}
