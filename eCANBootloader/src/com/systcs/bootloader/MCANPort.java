package com.systcs.bootloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import com.systcs.can.IXXAT;

public class MCANPort {
	
	public static enum ModelEvent {
		MODEL_CREATED,
		FILE_READ, BYTE_SENT, 
		SENDING_HEADER, SENDING_SECTION, SENDING_BLOCK, 
		FILE_READ_ERROR, FILE_PARSED, SENDING_BALANCE, HEADER_SENT, SECTION_SENT, CHECKSUM_READ
	}
	
	public static void postEvent(ModelEvent event) {
		CListener.onEvent(event);
	}
	
	public static int[] hexStream;
	public static byte[] data = new byte[1];
	public static int sent;
	public static int index;	//Index of byte in stream
	public static int sectionIndex;	//Index of section in file
	public static int blockIndex;	//Index of block when section is subdivided into blocks
	
	public static int headerSize = 22; //Header size in bytes
	public static int sectionHeaderSize = 6;	//Address (4 bytes) + size (2 bytes)
	public static int blockSize = 2048; //Block size in bytes
	public static int sleepTime = 150; //Sleep time in ms
	
	private static int sum;			//Used to generate checksum
	public static int myCheckSum; 	//Checksum that is computed when sending the data
	public static int checkSum;		//Checksum computed by receiver after getting the data
	
	public static int blockLength;
	public static int bytesToBeSent;
	public static int balanceBytes;
	
	public static class section {
		public int address;
		public int size;	
		public int index;
		section(int a, int s, int i) {
			address = a; size = s; index = i;
		}
	}
	
	public static ArrayList<section> sections = new ArrayList<section>(); 
	
	public static void readFile(String file) {
		
		File f = new File(file);
		int length = (int) f.length();
		hexStream = new int[length];
		
		try {
	    	 FileInputStream fin = new FileInputStream(file);
	    	 int current;
	    	 int i=0;
	         while (fin.available() > 0) {
	           current = fin.read();
	           hexStream[i++] = current;
	         } 
	    	 fin.close();
	    	 MCANPort.postEvent(ModelEvent.FILE_READ);
	    	 parseFile();
		}
	    catch (IOException ex) {
	    	 System.err.println(ex);
	    };
	}
	
	public static void parseFile() {
		
		balanceBytes = hexStream.length;
		index = headerSize;	//Skip header
		balanceBytes -= headerSize;
		
		while (index < hexStream.length-2) {	//2 bytes is EOF word
			int length = hexStream[index] + (hexStream[index+1] << 8);
			int address = (hexStream[index+3] << 24)
						+ (hexStream[index+2] << 16)
						+ (hexStream[index+5] << 8)
						+ (hexStream[index+4]);
			sections.add(new section(address, length,  index));
			index += (length*2 + 6);	//6 bytes of address and length overhead
			balanceBytes -= (length*2 + 6);
		}
		if (balanceBytes == 2) //Only 2 bytes of EOF left
			MCANPort.postEvent(ModelEvent.FILE_PARSED);
		else 
			MCANPort.postEvent(ModelEvent.FILE_READ_ERROR);
		
	}
	
	public static void loadHeader() {
		balanceBytes = hexStream.length;
		sum = 0;
		MCANPort.postEvent(ModelEvent.SENDING_HEADER);
		for (int i=0; i<headerSize; i++) {
			sent = hexStream[i];
			data[0] = (byte) sent;
			index = i;
			sendByte();
			sum += sent;
		}
		balanceBytes -= headerSize;
		myCheckSum = ~sum;
		MCANPort.postEvent(ModelEvent.HEADER_SENT);
		getResponse();
	}
	
	public static void loadSections() {
		for (section s: sections) {
			sectionIndex = sections.indexOf(s);
			sleep(sleepTime);
			loadSection(s);
		}
	}
	
	public static void loadSection(section s) {
		int from = s.index;
		bytesToBeSent = (s.size * 2);
		sum = 0;
		MCANPort.postEvent(ModelEvent.SENDING_SECTION);
		if (bytesToBeSent < blockSize) {
			int to = from + bytesToBeSent+sectionHeaderSize;
			for (int i=from; i<to; i++) {
				sent = hexStream[i];
				data[0] = (byte) sent;
				index = i;
				sendByte();	
				sum += sent;
			}
		}
		else {
			int nBlocks = bytesToBeSent/(blockSize);
			int balance = bytesToBeSent % (blockSize);
			for (int j=0; j<nBlocks; j++) {
				blockIndex = j;
				bytesToBeSent = blockSize;
				int f, t;
				if (j == 0) {	//First block
					f = s.index;
					t = f + blockSize+sectionHeaderSize;
				}
				else {
					f = s.index + sectionHeaderSize + j*blockSize;
					t = f + blockSize; 
				}
				MCANPort.postEvent(ModelEvent.SENDING_BLOCK);
				for (int i=f; i<t; i++) {
					sent = hexStream[i];
					data[0] = (byte) sent;
					index = i;
					sendByte();
					sum += sent;
				}
				sleep(sleepTime);
			}
			bytesToBeSent = balance;
			MCANPort.postEvent(ModelEvent.SENDING_BALANCE);
			int f = s.index + sectionHeaderSize + (nBlocks*blockSize);
			for (int i=f; i<f+balance; i++) {
				sent = hexStream[i];
				data[0] = (byte) sent;
				index = i;
				sendByte();	
				sum += sent;
			}	
		}
		myCheckSum = ~sum;
		MCANPort.postEvent(ModelEvent.SECTION_SENT);
		getResponse();
	}
	
	public static void sendByte() {
		//MCANPort.postEvent(ModelEvent.BYTE_SENT);
		if (CListener.onLine)
			writeToCAN(data);
	}
	
	public static void sleep(int msecs) {
		try {
			Thread.sleep(msecs);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static void writeToCAN(byte[] d) {
		IXXAT.write(0x00, d); //Broadcast
	}
	
	public static void bootload() {
		
		loadHeader();
		sleep(10000);	//Erase time of 10 secs
		loadSections();
		sleep(sleepTime);
		data[0] = (byte) (0x00);	//Send 2 0x00 bytes to end the process
		sendByte();
		sendByte();
	}
	
	private static void getResponse() {
		try {	//Short wait
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		byte[] resp = IXXAT.read();
		if (resp != null) {
			//Check if message is the checksum
			if (parseMessage(resp)) {
				MCANPort.postEvent(ModelEvent.CHECKSUM_READ);
			}
			else {
				;
			}
		}
	}
	
	public static boolean parseMessage(byte[] message) {
		int nBytes = message.length;
		if (nBytes != 4)
			return false;
		
		byte[] contents = new  byte[4];
		for (int i=0; i<4; i++)
			contents[i] = message[3-i]; //big-endian to little-endian swap is done here
		checkSum = new BigInteger(contents).intValue();
		return true;
	}

}
