package com.systcs.bootloader;

import com.systcs.bootloader.MCANPort.ModelEvent;
import com.systcs.bootloader.VDesktop.ViewEvent;
import com.systcs.can.IXXAT;
import com.systcs.can.IXXAT.CANDriverEvent;


public class CListener {
	
	public static boolean onLine = true;		//Used when testing is without a CAN network connected 
	//User requests are responded to with data stored in the MJoint
	//Make = true for use with CAN network

	static VDesktop desktop; 	//MVC architecture View - only communicates with Listener
	static MCANPort port;		//MVC architecture Model - only communicates with Listener
	
	CListener(VDesktop d) {
	desktop = d;
	}
	
	public void setModel(MCANPort m) {
	port = m;
	}
	
	static public void onEvent(ViewEvent event) {
		
		switch (event) {
		case APP_LAUNCHED:
			VDesktop.log("Application Launched\n");
			CRoot.setupCANPort();
			break;
		case APP_CLOSING:
			IXXAT.close();
			VDesktop.log("Application Closed\n");
			break;
		case CAN_OPEN_REQUESTED:
			VDesktop.log("Opening CAN communication...\n");
			IXXAT.open();
			break;
		case CAN_TEST_REQUESTED:
			VDesktop.log("Testing CAN communication...\n");
			IXXAT.test();
			break;
		case CAN_CLOSE_REQUESTED:
			VDesktop.log("Closing CAN Communication...\n");
			break;
		case APP_CLOSE_REQUESTED:
			desktop.close();
			break;
		case FILE_SELECTED:
			MCANPort.readFile(VDesktop.fileSelected);
			break;
		case LOAD_REQUESTED:
			VDesktop.log("\nStreaming data over CAN:");
			MCANPort.bootload();
			break;
		default:
			break;
		}
	}
	
	static public void onEvent(CANDriverEvent event) {
		
		switch (event) {
		case CAN_EXCEPTION:
			VDesktop.log("Exception in CAN Initialization\n");
			desktop.retryCanOpen();
			break;
		case CAN_OPENED:
			VDesktop.log("CAN Communications Open\n");
			//VDesktop.postEvent(ViewEvent.CAN_TEST_REQUESTED);
			break;
		case CAN_TEST_PASSED:
			VDesktop.log("CAN Test passed\n");
			//desktop.activate();
			break;
		case CAN_TEST_FAILED:
			VDesktop.log("CAN Test failed\n");
			desktop.retryCanTest();
			break;
		case CAN_READ_FAILED:
			VDesktop.log("CAN Read failed\n");
			break;
		case CAN_READ_PASSED:
			VDesktop.log("CAN Read passed\n");
			break;
		case CAN_WRITE_DONE:
			//VDesktop.log("CAN Message Written\n");
			break;
		case CAN_MSG_FAILED:
			//VDesktop.log.append("CAN Message Read failed\n");
			break;
		default:
			break;
			
		}
		
	}

	static public void onEvent(ModelEvent event) {
	
		switch (event) {
			case MODEL_CREATED:
				VDesktop.log("Model created\n");
				break;
			case FILE_READ:
				VDesktop.enableLoad();
				break;
			case FILE_PARSED:
				VDesktop.log("\n File structure is:\n");
				VDesktop.log("Section   Address            Length            Bytes\n");
				for (int i=0; i<MCANPort.sections.size(); i++) {
					int address = MCANPort.sections.get(i).address;
					int length =  MCANPort.sections.get(i).size;
					int bytes = length*2 + 6;
					VDesktop.log(String.format("    %d     0x%08x    0x%08x     %8d \n", i, address, length, bytes));
				}
				break;
			case SENDING_HEADER:
				VDesktop.log("\n\nSending header\n");
				break;
			case SENDING_SECTION:
				VDesktop.log(String.format("\n\nSending section: %d\n", MCANPort.sectionIndex+1));
				VDesktop.log(String.format("Sending %d bytes\n", MCANPort.bytesToBeSent));
				break;
			case SENDING_BLOCK:
				VDesktop.log(String.format("\n\nSending block: %d\n", MCANPort.blockIndex+1));
				VDesktop.log(String.format("Sending %d bytes\n", MCANPort.bytesToBeSent));
				break;
			case SENDING_BALANCE:
				VDesktop.log("\n\nSending balance:\n");
				VDesktop.log(String.format("Sending %d bytes\n", MCANPort.bytesToBeSent));
				break;
			case BYTE_SENT:
				if ((MCANPort.index % 16) == 0) //Line return after 16 columns
					VDesktop.log("\n");
				VDesktop.displayByte(MCANPort.sent);
				break;
			case HEADER_SENT:
				VDesktop.log(String.format("\n Header checksum is: 0x%08x", MCANPort.myCheckSum));
				break;
			case SECTION_SENT:
				VDesktop.log(String.format("\n Section checksum is: 0x%08x", MCANPort.myCheckSum));
				break;
			case CHECKSUM_READ:
				VDesktop.log(String.format("\n Received checksum is: 0x%08x", MCANPort.checkSum));
				if (MCANPort.checkSum != MCANPort.myCheckSum)
					VDesktop.log("\n ****** CHECKSUM ERROR ******");
				break;
			case FILE_READ_ERROR:
				VDesktop.log("\n\nBlock Read Error:\n");
				VDesktop.log(String.format("Block length is: %d \n", MCANPort.blockLength));
				VDesktop.log(String.format("Balance Byte count is: %d \n", MCANPort.balanceBytes));
				break;
			default:
				break;
		}
	}
}


